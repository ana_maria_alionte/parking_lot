package com.practice.parking.dao;

import java.util.List;

import com.practice.parking.model.Parking;

public interface ParkingDAO {

	public List<Parking> getAvailableParkingLot(String... criteria);

	public void setAvailability(int id);

}
